# Diamond Square Algorithm

This project contains the source code for the Diamond Square Algorithm explained [here](https://asyncdrink.com/blog/diamond-square-algorithm).

## Dependencies

The following dependencies are required to run and build this project:
- Node v10
- NPM v6

Before doing anything make sure all Node packages are installed:

```
npm install
```

## Building

To build the project do:

```
npm run build
```

To build the project with a watch do:

```
npx tsc -w
```

## Running

To run the project do:

```
npm run start
```

Next navigate to [http://localhost:8080](http://localhost:8080) to view the project.
