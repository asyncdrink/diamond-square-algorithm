export class Grid {
    private values: number[][];

    /**
     * Constructs a new grid.
     * @param size The size (e.g. width and height) of the grid.
     */
    constructor(private size: number) {
        this.clear();
    }

    /**
     * Reset the grid so all height values are 0.
     */
    public clear() {
        this.values = [];

        for(let i = 0; i < this.size; i++) {
            this.values.push(
                new Array(this.size).fill(NaN, 0, this.size)
            )
        }
    }

    /**
     * Sets the given value at the given coordinate.
     * @param value 
     * @param x 
     * @param y 
     */
    public setValue(value: number, x: number, y: number) {
        if (!this.isInGrid(x, y)) {
            throw new Error(`Coordinates outside of grid: (${ x }, ${ y })`);
        }

        this.values[y][x] = value;
    }

    /**
     * Gets the value of the given coordinate.
     * @param x 
     * @param y 
     */
    public getValue(x: number, y: number): number {
        if (!this.isInGrid(x, y)) {
            throw new Error(`Coordinates outside of grid: (${ x }, ${ y })`);
        }

        return this.values[y][x];
    }

    /**
     * Returns the size of the grid. The size applies to both the width and height.
     */
    public getSize(): number {
        return this.size;
    }

    /**
     * Returns true if the given coordinates lies within the grid.
     * @param x 
     * @param y 
     */
    public isInGrid(x: number, y: number): boolean {
        return (x >= 0 && x < this.size) &&
               (y >= 0 && y < this.size);
    }

    /**
     * Smooths the grid.
     */
    public smooth() {
        const possibleIndexes = [
            {x: -1, y: -1},
            {x: -1, y: 0},
            {x: -1, y: 1},
            {x: 0, y: -1},
            {x: 0, y: 0},
            {x: 0, y: 1},
            {x: 1, y: -1},
            {x: 1, y: 0},
            {x: 1, y: 1}
        ];
        
        for(let x = 0; x < this.size; x++) {
            for(let y = 0; y < this.size; y++) {
                const indices = possibleIndexes.filter(
                    (value) => this.isInGrid(x + value.x, y + value.y)
                );

                const sum = indices.reduce(
                    (previous, current) => previous + this.values[y + current.y][x + current.x],
                    0
                );

                this.values[y][x] = sum / indices.length;
            }
        }
    }
}