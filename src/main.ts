import { Grid } from './grid.js';
import { DiamondSquareGenerator } from './diamond-square-generator.js';

window.onload = () => {
    const grid = new Grid(Math.pow(2, 8) + 1);
    const scale = 4;

    const canvas: HTMLCanvasElement = document.getElementById('canvas') as HTMLCanvasElement;
    
    canvas.width = grid.getSize() * scale;
    canvas.height = grid.getSize() * scale;
    
    const context = canvas.getContext('2d');

    const generator = new DiamondSquareGenerator(grid);
    generator.generate();

    context.scale(scale, scale);

    for(let x = 0; x < grid.getSize(); x++) {
        for(let y = 0; y < grid.getSize(); y++) {
            const value = grid.getValue(x, y);

            let color: string;
            if (value < 0.4) {
                color = 'rgb(0, 0, 255)';
            } else if(value >= 0.4 && value < 0.41) {
                color = 'rgb(160, 160, 9)';
            } else if(value >= 0.41 && value < 0.7) {
                color = 'rgb(0, 255, 0)';
            } else if(value >= 0.7 && value <= 0.95) {
                color = 'rgb(64, 192, 64)';
            } else if(value >= 0.95 && value < 0.98) {
                color = 'rgb(128, 128, 128)';
            } else if(!isNaN(value)) {
                color = 'rgb(255, 255, 255)';
            }

            context.fillStyle = color;
            context.fillRect(x, y, 1, 1);
        }
    }
}
