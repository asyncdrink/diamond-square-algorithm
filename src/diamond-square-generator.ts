import { Grid } from "./grid.js";

export class DiamondSquareGenerator {
    constructor(private grid: Grid) {}

    generate() {
        this.grid.clear();

        this.initialize();

        this.step(this.grid.getSize());

        this.grid.smooth();
    }

    /**
     * Initializes the grid by setting the four corner points.
     */
    private initialize() {
        this.grid.setValue(Math.random(), 0, 0);
        this.grid.setValue(Math.random(), this.grid.getSize() - 1, 0);
        this.grid.setValue(Math.random(), this.grid.getSize() - 1, this.grid.getSize() - 1);
        this.grid.setValue(Math.random(), 0, this.grid.getSize() - 1);
    }

    /**
     * Performs a single step of the Diamond Square Algorithm at the given scale.
     * 
     * This function will recursively call itself until no more steps can be done.
     * @param scale
     */
    private step(scale: number) {
        if (scale <= 2) {
            return;
        }

        this.diamond(scale);
        this.square(scale);

        this.step(Math.ceil(scale / 2));
    }

    /**
     * Performs the 'square' step of the Diamond Square Algorithm at the given scale.
     * @param scale
     */
    private diamond(scale: number) {
        const steps = Math.floor(this.grid.getSize() / (scale - 1));
        const magnitude = scale / this.grid.getSize();

        // Do square step
        for(let x = 0; x < steps; x++) {
            for(let y = 0; y < steps; y++) {
                const xCoord = x * (scale - 1);
                const yCoord = y * (scale - 1);

                const maxLength = scale - 1;

                const values = [
                    this.grid.getValue(xCoord, yCoord),
                    this.grid.getValue(xCoord + maxLength, yCoord),
                    this.grid.getValue(xCoord + maxLength, yCoord + maxLength),
                    this.grid.getValue(xCoord, yCoord + maxLength),
                ];

                const average = this.getAverage(values);

                this.grid.setValue(
                    average + this.getRandomValue(magnitude),
                    xCoord + (scale >> 1), yCoord + (scale >> 1)
                );
            }
        }
    }

    /**
     * Performs the 'diamond' step of the Diamond Square Algorithm at the given scale.
     * @param scale 
     */
    private square(scale: number) {
        const steps = Math.floor(this.grid.getSize() / (scale - 1));
        const magnitude = scale / this.grid.getSize();

        // Do diamond step
        for(let x = 0; x < steps; x++) {
            for(let y = 0; y < steps; y++) {
                const xCoord = x * (scale - 1);
                const yCoord = y * (scale - 1);

                const maxLength = scale - 1;
                const halfSize = scale >> 1;

                // Top
                {
                    const values: number[] = [
                        this.grid.getValue(xCoord, yCoord),
                        this.grid.getValue(xCoord + maxLength, yCoord),
                        this.grid.getValue(xCoord + halfSize, yCoord + halfSize)
                    ];

                    if (this.grid.isInGrid(xCoord + halfSize, yCoord - halfSize)) {
                        values.push(this.grid.getValue(xCoord + halfSize, yCoord - halfSize));
                    }

                    const average = this.getAverage(values);

                    this.grid.setValue(
                        average + this.getRandomValue(magnitude),
                        xCoord + halfSize, yCoord
                    );
                }

                // bottom
                {
                    const values: number[] = [
                        this.grid.getValue(xCoord, yCoord + maxLength),
                        this.grid.getValue(xCoord + maxLength, yCoord + maxLength),
                        this.grid.getValue(xCoord + halfSize, yCoord + halfSize)
                    ];

                    if (this.grid.isInGrid(xCoord + halfSize, yCoord + maxLength + halfSize)) {
                        values.push(this.grid.getValue(xCoord + halfSize, yCoord + maxLength + halfSize));
                    }

                    const average = this.getAverage(values);

                    this.grid.setValue(
                        average + this.getRandomValue(magnitude),
                        xCoord + halfSize, yCoord + maxLength
                    );
                }

                // left
                {
                    const values: number[] = [
                        this.grid.getValue(xCoord, yCoord),
                        this.grid.getValue(xCoord, yCoord + maxLength),
                        this.grid.getValue(xCoord + halfSize, yCoord + halfSize)
                    ];

                    if (this.grid.isInGrid(xCoord - halfSize, yCoord + halfSize)) {
                        values.push(this.grid.getValue(xCoord - halfSize, yCoord + halfSize));
                    }

                    const average = this.getAverage(values);

                    this.grid.setValue(
                        average + this.getRandomValue(magnitude),
                        xCoord, yCoord + halfSize
                    );
                }

                // right
                {
                    const values: number[] = [
                        this.grid.getValue(xCoord + maxLength, yCoord),
                        this.grid.getValue(xCoord + maxLength, yCoord + maxLength),
                        this.grid.getValue(xCoord + halfSize, yCoord + halfSize)
                    ];

                    if (this.grid.isInGrid(xCoord + maxLength + halfSize, yCoord + halfSize)) {
                        values.push(this.grid.getValue(xCoord + maxLength + halfSize, yCoord + halfSize));
                    }

                    const average = this.getAverage(values);

                    this.grid.setValue(
                        average + this.getRandomValue(magnitude),
                        xCoord + maxLength, yCoord + halfSize
                    );
                }
            }
        }
    }

    /**
     * Returns a random number for the given magnitude.
     * @param magnitude 
     */
    private getRandomValue(magnitude: number): number {
        return Math.random() * magnitude - (magnitude / 2);
    }

    /**
     * Calculates the average of the given numbers.
     * @param values 
     */
    private getAverage(values: number[]): number {
        const sum = values.reduce((previous, current) => previous + current, 0);

        return sum / values.length;
    }
}